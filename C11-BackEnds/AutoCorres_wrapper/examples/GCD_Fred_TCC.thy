(******************************************************************************
 * Isabelle/C/AutoCorres
 *
 * Authors : Burkhart Wolff, Frederic Tuong, Frederic Boulanger.
 * 
 * Copyright (c) 2020 Université Paris-Saclay, LRI, France
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *
 *     * Neither the name of the copyright holders nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************)

theory GCD_Fred_TCC
  imports   Isabelle_C_AutoCorres.AutoCorres_Wrapper
begin

text\<open>This is Fred's GCD example in TCC (Theories carrying Code) - style. \<close>


section\<open>Background\<close>

text\<open>No further background other than the HOL-library necessary; in particular, 
     this program theory resides on the definition of @{term "GCD.gcd"}.\<close>




section\<open>The Program\<close>

declare [[AutoCorres]]

text \<open>Setup of AutoCorres for semantically representing this C element:\<close>

declare_autocorres pgcd [ ts_rules = nondet, unsigned_word_abs = pgcd ]


C\<open> 
   int pgcd(int m, int n) {
     int a = m;
     int b = n;
     while (a != b) {
       if (a < b) {
         b = b - a;
       } else {
         a = a - b;
       }
     }
     return a;
   }
 \<close>

print_theorems
find_theorems name:pgcd





section\<open>The Correctness-Proof\<close>

text\<open>We wrap up the invariant explicitely in this predicate:\<close>
definition "pgcd_inv (a::int) b m n \<equiv> a > 0 \<and> b > 0 \<and>  
                                      m < INT_MAX \<and> n < INT_MAX \<and> 
                                      a \<le> m \<and> b \<le> n \<and>
                                      (\<forall> g. g = gcd a b \<longrightarrow> g = gcd m n)"



text\<open>... and state the correctness problem as Hoare-Triple. And there we go:\<close>
theorem (in pgcd) pgcd'_correct:
    "\<lbrace> \<lambda>\<sigma>. m > 0 \<and> n > 0 \<and> m < INT_MAX \<and> n < INT_MAX \<rbrace> pgcd' m n \<lbrace> \<lambda>res \<sigma>. res = gcd m n \<rbrace>!"

  unfolding pgcd'_def
  text\<open>Turning precond into explicit assumption\<close>
  apply(rule validNF_assume_pre)
  text\<open>Annotating loop with invariant and measure\<close>
  apply(subst whileLoop_add_inv   [  where I = "\<lambda>(a, b) s. pgcd_inv a b m n"
                                     and   M = "\<lambda>((a, b),s) . nat a + nat b"])

  proof(wp, fold INT_MAX_def, auto simp: pgcd_inv_def)

      fix a b  show " gcd a b = gcd m n \<Longrightarrow> gcd a (b - a) = gcd m n"
        by (metis gcd.commute gcd_diff1)

  next

      fix a b  show " gcd a b = gcd m n \<Longrightarrow> gcd (a - b) b = gcd m n"
        by (simp add: gcd_diff1)

  qed

end

