theory GCD_Fred_CAS
  imports   Isabelle_C_AutoCorres.AutoCorres_Wrapper
begin

text\<open>This is Fred's GCD example\<close>

text\<open>For reflection \<^file>\<open>../../../../Isabelle_C/src_ext/l4v/src/tools/autocorres/AutoCorres.thy\<close>

     Critical for a hook:
     - \<^file>\<open>../../../../Isabelle_C/src_ext/l4v/src/tools/autocorres/type_strengthen.ML\<close>

     \<^ML_structure>\<open>TypeStrengthen\<close>
\<close>


section \<open>Setup of Representation Function \<open>C\<^sub>e\<^sub>x\<^sub>p\<^sub>r\<close>\<close>


setup \<open>C_Module.C_Term.map_expression
        (fn expr => fn _ => fn _ => 
          case expr of C_Ast.CVar0 (C_Ast.Ident0 (_, x, _), _) =>
                         Free (C_Grammar_Rule_Lib.ident_decode x, dummyT)
                     | s => Free (\<^make_string> s, dummyT))\<close>



section\<open>Background\<close>

text\<open>No further background other than the HOL-library necessary; in particular, 
     this program theory resides on the definition of @{term "GCD.gcd"}.\<close>



section\<open>The Program\<close>

declare [[AutoCorres]]

text \<open>Setup of AutoCorres for semantically representing this C element:\<close>


definition "pgcd_inv (a::int) b m n \<equiv> a > 0 \<and> b > 0 \<and>  
                                      m < INT_MAX \<and> n < INT_MAX \<and> 
                                      a \<le> m \<and> b \<le> n \<and>
                                      (\<forall> g. g = gcd a b \<longrightarrow> g = gcd m n)"



ML\<open>

fun definition (((decl, spec), prems), params) =
  #2 oo Specification.definition_cmd decl params prems spec;

fun definition2 (decl, spec, prems, params) = #2 oo Specification.definition' decl params prems spec



val X = (Scan.option Parse_Spec.constdecl -- (Parse_Spec.opt_thm_name ":" -- Parse.prop) --
        Parse_Spec.if_assumes -- Parse.for_fixes) 
        : ((((binding * string option * mixfix) option * (Attrib.binding * string)) * string list) *
      (binding * string option * mixfix) list) parser;

C_Inner_Syntax.local_command''
      ("definition", \<^here>, \<^here>, \<^here>)
      (Scan.option Parse_Spec.constdecl -- (Parse_Spec.opt_thm_name ":" -- Parse.prop) --
        Parse_Spec.if_assumes -- Parse.for_fixes)
      definition;

C_Inner_Syntax.local_command';
C_Inner_Syntax.command0;
C_Inner_Syntax.command00;
C_Inner_Syntax.command_no_range;
C_Inner_Syntax.command_no_range';
C_Inner_Syntax.command_no_range';
\<close>


ML\<open>val H : Input.source -> Inttab.key option -> Context.generic -> Context.generic =
              C_Inner_Toplevel.generic_theory oo C_Inner_Isar_Cmd.setup\<close>

ML\<open>
fun  mk_undefined (@{typ "unit"}) = Const (\<^const_name>\<open>Product_Type.Unity\<close>, \<^typ>\<open>unit\<close>)
    |mk_undefined t               = Const (\<^const_name>\<open>HOL.undefined\<close>, t)

fun meta_eq_const T = Const (\<^const_name>\<open>Pure.eq\<close>, T --> T --> propT);

fun mk_meta_eq (t, u) = meta_eq_const (fastype_of t) $ t $ u;

fun   mk_pat_tupleabs [] t = t
    | mk_pat_tupleabs [(s,ty)] t = absfree(s,ty)(t)
    | mk_pat_tupleabs ((s,ty)::R) t = HOLogic.mk_case_prod(absfree(s,ty)(mk_pat_tupleabs R t));


\<close>

ML\<open>


   fun define_cond binding f_sty transform_old  cond_suffix params read_cond (ctxt:local_theory) = 
       let val params' = map (fn(b, ty) => (Binding.name_of b,ty)) params
           val src' = case transform_old (read_cond ctxt) of 
                        Abs(nn, sty_pre, term) => mk_pat_tupleabs params' (Abs(nn,sty_pre,term))
                      | _ => error ("define abstraction for result" ^ Position.here \<^here>)
           val bdg = Binding.suffix_name cond_suffix binding
           val bdg_ty = HOLogic.mk_tupleT(map (#2) params) --> f_sty HOLogic.boolT
           val eq =  mk_meta_eq(Free(Binding.name_of bdg, bdg_ty),src')
           val args = (SOME(bdg,NONE,NoSyn), (Binding.empty_atts,eq),[],[]) 
       in  definition2 args true ctxt end

   fun define_precond binding sty =
       define_cond binding (fn boolT => sty --> boolT) I  "_requires" 

   fun define_postcond binding rty sty =
       define_cond binding (fn boolT => sty --> sty --> rty --> boolT) I "_ensures" 

 
 \<close>

ML\<open>
local 
val CMD = ref(K (K I):LALR_Table.state -> C_Env.env_lang -> Context.generic -> Context.generic);
in
fun command_general cmd = let val _ = (CMD:=cmd) in
         (H
         \<open>(fn ((tb:LALR_Table.state, (X : C_Grammar_Rule.svalue0, pos1, pos2)) :: Z) =>
              (fn stck (* Polytypic - depends on the navigator , for example for +@: 
                          : 'a C_Stack.stack_elem *) => 
                 fn cenv: C_Env.env_lang =>
                tap (fn ctxt : Context.generic => !CMD tb cenv ctxt))
           | _ => fn _ => fn _ => I )\<close>) 
         end
         ;  
end
\<close>

text\<open>@{ML_type "C_Ast.CTypeSpec C_Stack.stack_elem"}\<close>

ML\<open>
val Highlight = (H
         \<open>(fn ((tb:LALR_Table.state, (X : C_Grammar_Rule.svalue0, pos1, pos2)) :: Z) =>
              (fn stck (* Polytypic - depends on the navigator , for example 
                          for +@:  C_Ast.CTypeSpec C_Stack.stack_elem 
                          for ++@: C_Ast.CDeclrR C_Stack.stack_elem*)
                           => 
                 fn cenv: C_Env.env_lang =>
                tap (fn ctxt : Context.generic => (          
                      Position.reports_text [((Position.range (pos1, pos2)
                                               |> Position.range_position, Markup.intensify), "")])))
           | _ => fn _ => fn _ => I )\<close>) 
         ;  
(*
fun command' cmd scan f =
       C_Annotation.command' cmd "" (bind_export scan f)

fun command'' cmd A B C =
    command' cmd
           (C_Token.syntax' (Parse.token Parse.term))
           (fn src => fn _ => fn ctxt =>
             ML_Context.exec
               (tap (fn _ => Syntax.read_term (Context.proof_of ctxt)
                                              (Token.inner_syntax_of src)))
               ctxt)



val _ = command' : string * Position.T ->
      (binding option * string) C_Parse.parser ->
        (binding option * string -> C_Env.env_propagation_reduce -> Context.generic -> Context.generic)
          -> theory -> theory
*)

val _ = Theory.setup   (   C_Inner_Syntax.command_no_range Highlight  ("highlight2", \<^here>, \<^here>) )

(*
                        #> command' ("ensures2", \<^here>)   scan_lab_inner_syntax ensures)
                       
*)
\<close>







C\<open>

int x;

int pgcd(int m, int n) {
  //@ definition \<open>pgcd_requires m n = (\<lambda> \<sigma>.  m>0 \<and> n > 0 \<and> m \<le> UINT_MAX  \<and> n \<le> UINT_MAX)\<close>
  //@ +@ requires pre1 : \<open>\<^C>\<^sub>e\<^sub>x\<^sub>p\<^sub>r\<open>m\<close> > 0 \<and> \<^C>\<^sub>e\<^sub>x\<^sub>p\<^sub>r\<open>n\<close>  > 0 \<and>  \<^C>\<^sub>e\<^sub>x\<^sub>p\<^sub>r\<open>m\<close> \<le> UINT_MAX  \<and> \<^C>\<^sub>e\<^sub>x\<^sub>p\<^sub>r\<open>n\<close> \<le> UINT_MAX\<close>
  // the clause should be as above ...
  //@ definition "pgcd_ensures m n = (\<lambda>res \<sigma>.  res = gcd m n)"
  //@ +@ ensures post1 :  \<open>\<lambda>res. res = gcd \<^C>\<^sub>e\<^sub>x\<^sub>p\<^sub>r\<open>m\<close>  \<^C>\<^sub>e\<^sub>x\<^sub>p\<^sub>r\<open>n\<close> \<close>
  // the clause should be as above ...
  //@ ++@ highlight2

  int a = m;
  int b = n;

  while (a != b) {
  //@ definition "pgcd_inv\<^sub>1    m n = (\<lambda>(a, b) _. pgcd_inv a b m n)"
  //@ definition "pgcd_mesure\<^sub>1 m n = (\<lambda>((a, b),_) . nat a + nat b)"
  //@ INVARIANT : "\<lambda>(a, x) s. pgcd_inv a b \<^C>\<^sub>e\<^sub>x\<^sub>p\<^sub>r\<open>m\<close>  \<^C>\<^sub>e\<^sub>x\<^sub>p\<^sub>r\<open>n\<close>"
  //@ invariant  \<comment> \<open>inner\<close> \<open>\<lambda>(a, y) s. pgcd_inv a b \<^C>\<^sub>e\<^sub>x\<^sub>p\<^sub>r\<open>m\<close>  \<^C>\<^sub>e\<^sub>x\<^sub>p\<^sub>r\<open>n\<close> \<close>
  //@ measure    \<comment> \<open>inner\<close> \<open>\<lambda>((a, z),s) . nat a + nat b\<close>
    if (a < b) {
      b = b - a;
    } else {
      a = a - b;
    }
  }
  return a;
}

//@ install_autocorres pgcd [ ts_rules = nondet, unsigned_word_abs = pgcd ]

\<close>

ML\<open>Inttab.lookup;
C_Module'.Annotation_Data.get;
C_Module'.Annotation_Data.put;
C_Module'.Annotation_Data.map;
 \<close>

ML\<open>
val get_anno_data = C_Module'.Annotation_Data.get ;
val bbb = get_anno_data (Context.Proof @{context})
\<close>



ML\<open>
val get_anno_data = C_Module'.Annotation_Data.get ;
val blocks = Inttab.keys o get_anno_data;

val get_block  =  (the oo Inttab.lookup) o  get_anno_data; (* polymorphism *)
fun swapC f a b = f b a;
fun filter_tag_of_block (k,tag) ad = ad |> (swapC get_block) k
                                        |> filter (fn X => tag = #tag X)
val get_invariants = filter_tag_of_block (1,"invariant")
val get_measures   = filter_tag_of_block (1,"measure")
val get_requires   = filter_tag_of_block (1,"requires")
val get_ensures    = filter_tag_of_block (1,"ensures")

val a = get_requires (Context.Proof @{context})
\<close>

ML\<open>
val [(n,[A,B,C,D])] = Inttab.dest bbb;
val tag1 = (#tag A);
val t1 = Syntax.read_term @{context} (#content A);
val t2 = Syntax.read_term @{context} (#content B);
val Env1 = (#c_env A)
val Env2 = (#c_env B)

fun get_symtab       (env : C_Env.env_lang) = (#idents o snd o hd (* ? *) o #scopes) env
fun get_symtab_ids   (env : C_Env.env_lang) = Symtab.keys(get_symtab env)
fun get_symtab_global_ids(env : C_Env.env_lang) =
                     map_filter (fn(x,y) => if (#global o #3) y then SOME x else NONE)
                                (Symtab.dest(get_symtab env))

fun symtab_lookup_id (env : C_Env.env_lang) (id:string) =
                            the(Symtab.lookup (get_symtab env) id)

val {  var_table 
     , scopes : (C_Ast.ident option * C_Env.var_table) list 
     , namesupply : int
     , stream_ignored : C_Antiquote.antiq C_Env.stream
     , env_directives : C_Env.env_directives } = (#c_env A)\<close>

ML\<open> \<close>

ML\<open>(*get_symtab_ids (#c_env A); *)

   get_symtab_global_ids (#c_env A);

   (* symtab_lookup_id (#c_env A) "n" *)
  val params = #params(#3(symtab_lookup_id (#c_env A) "pgcd"));
  val [C_Ast.CFunDeclr0 X] = params;
  val (Y1,Y2,Y3) = X ;
  val C_Ast.Right Y4 = Y1;

val ([C_Ast.CDecl0 Y5,C_Ast.CDecl0 Y5'],false) =Y4;
val (Y6,Y6',Y6'') = Y5

val [C_Ast.CTypeSpec0 Y7] = Y6
val  C_Ast.CIntType0 (C_Ast.NodeInfo0 Y8) = Y7\<close>


(* HINT *)
ML\<open>@{make_string} Y8\<close>



ML\<open>

val nodes = fold(C11_Ast_Lib.fold_cDerivedDeclarator 
                (fn _ => fn a => fn S => a :: S)) params []

val S = map C11_Ast_Lib.toString_nodeinfo nodes;

     \<close>

ML\<open>open Char\<close>

(*
ML\<open>

signature C11_AST_LIB =
  sig
    (* some general combinators *)
    val fold_either: ('a -> 'b -> 'c) -> ('d -> 'b -> 'c) -> ('a, 'd) C_Ast.either -> 'b -> 'c
    val fold_optiona: ('a -> 'b -> 'b) -> 'a C_Ast.optiona -> 'b -> 'b

    datatype data = data_bool of bool     | data_int of int 
                  | data_string of string | data_absstring of string 

    type node_content = { tag     : string,
                          sub_tag : string,
                          args    : data list }

    (* conversions of enumeration types to string codes *)
    val toString_cBinaryOp : C_Ast.cBinaryOp -> string
    val toString_cIntFlag  : C_Ast.cIntFlag -> string
    val toString_cIntRepr  : C_Ast.cIntRepr -> string
    val toString_cUnaryOp  : C_Ast.cUnaryOp -> string
    val toString_cAssignOp : C_Ast.cAssignOp -> string
    val toString_abr_string: C_Ast.abr_string -> string
    val toString_nodeinfo  : C_Ast.nodeInfo -> string


    (* a generic iterator collection over the entire C11 - AST. The lexical 
       "leaves" of the AST's are parametric ('a). THe collecyot function "g" (see below)
       gets as additional parameter a string-key representing its term key
       (and sometimes local information such as enumeration type keys). *)
    (* Caveat : Assembly is currently not supported *)
    (* currently a special case since idents are not properly abstracted in the src files of the
       AST generation: *)
    val fold_ident: 'a -> (node_content -> 'a -> 'b -> 'c) -> C_Ast.ident -> 'b -> 'c
    (* the "Leaf" has to be delivered from the context, the internal non-parametric nodeinfo
       is currently ignored. HACK. *)

    val fold_cInteger: (node_content -> 'a -> 'b) -> C_Ast.cInteger -> 'a -> 'b
    val fold_cConstant: (node_content -> 'a -> 'b -> 'b) -> 'a C_Ast.cConstant -> 'b -> 'b
    val fold_cStringLiteral: (node_content -> 'a -> 'b -> 'b) -> 'a C_Ast.cStringLiteral -> 'b -> 'b


    val fold_cArraySize: 'a -> (node_content -> 'a -> 'b -> 'b) -> 'a C_Ast.cArraySize -> 'b -> 'b
    val fold_cAttribute: (node_content -> 'a -> 'b -> 'b) -> 'a C_Ast.cAttribute -> 'b -> 'b
    val fold_cBuiltinThing: (node_content -> 'a -> 'b -> 'b) -> 'a C_Ast.cBuiltinThing -> 'b -> 'b
    val fold_cCompoundBlockItem: (node_content -> 'a -> 'b -> 'b) 
                                    -> 'a C_Ast.cCompoundBlockItem -> 'b -> 'b
    val fold_cDeclaration: (node_content -> 'a -> 'b -> 'b) -> 'a C_Ast.cDeclaration -> 'b -> 'b
    val fold_cDeclarationSpecifier: (node_content -> 'a -> 'b -> 'b) 
                                    -> 'a C_Ast.cDeclarationSpecifier -> 'b -> 'b
    val fold_cDeclarator: (node_content -> 'a -> 'b -> 'b) -> 'a C_Ast.cDeclarator -> 'b -> 'b
    val fold_cDerivedDeclarator: (node_content -> 'a -> 'b -> 'b) 
                                    -> 'a C_Ast.cDerivedDeclarator -> 'b -> 'b
    val fold_cEnumeration: (node_content -> 'a -> 'b -> 'b) -> 'a C_Ast.cEnumeration -> 'b -> 'b
    val fold_cExpression: (node_content -> 'a -> 'b -> 'b) -> 'a C_Ast.cExpression -> 'b -> 'b
    val fold_cInitializer: (node_content -> 'a -> 'b -> 'b) -> 'a C_Ast.cInitializer -> 'b -> 'b
    val fold_cPartDesignator: (node_content -> 'a -> 'b -> 'b) -> 'a C_Ast.cPartDesignator -> 'b -> 'b
    val fold_cStatement: (node_content -> 'a -> 'b -> 'b) -> 'a C_Ast.cStatement -> 'b -> 'b
    val fold_cStructureUnion : (node_content -> 'a -> 'b -> 'b) -> 'a C_Ast.cStructureUnion -> 'b -> 'b 
    val fold_cTypeQualifier: (node_content -> 'a -> 'b -> 'b) -> 'a C_Ast.cTypeQualifier -> 'b -> 'b
    val fold_cTypeSpecifier: (node_content -> 'a -> 'b -> 'b) -> 'a C_Ast.cTypeSpecifier -> 'b -> 'b

  (* universal sum type : *)

    datatype 'a C11_Ast = 
           mk_cInteger              of    C_Ast.cInteger         
         | mk_cConstant             of 'a C_Ast.cConstant      
         | mk_cStringLiteral        of 'a C_Ast.cStringLiteral 
         | mk_cArraySize            of 'a C_Ast.cArraySize     
         | mk_cAttribute            of 'a C_Ast.cAttribute     
         | mk_cBuiltinThing         of 'a C_Ast.cBuiltinThing  
         | mk_cCompoundBlockItem    of 'a C_Ast.cCompoundBlockItem   
         | mk_cDeclaration          of 'a C_Ast.cDeclaration         
         | mk_cDeclarationSpecifier of 'a C_Ast.cDeclarationSpecifier
         | mk_cDeclarator           of 'a C_Ast.cDeclarator          
         | mk_cDerivedDeclarator    of 'a C_Ast.cDerivedDeclarator   
         | mk_cEnumeration          of 'a C_Ast.cEnumeration         
         | mk_cExpression           of 'a C_Ast.cExpression          
         | mk_cInitializer          of 'a C_Ast.cInitializer         
         | mk_cPartDesignator       of 'a C_Ast.cPartDesignator      
         | mk_cStatement            of 'a C_Ast.cStatement           
         | mk_cStructureUnion       of 'a C_Ast.cStructureUnion
         | mk_cTypeQualifier        of 'a C_Ast.cTypeQualifier 
         | mk_cTypeSpecifier        of 'a C_Ast.cTypeSpecifier 
         | mk_cStructTag            of C_Ast.cStructTag 
         | mk_cUnaryOp              of C_Ast.cUnaryOp  
         | mk_cAssignOp             of C_Ast.cAssignOp 
         | mk_cBinaryOp             of C_Ast.cBinaryOp 
         | mk_cIntFlag              of C_Ast.cIntFlag  
         | mk_cIntRepr              of C_Ast.cIntRepr  



  end


structure C11_Ast_Lib  : C11_AST_LIB   =
struct 
local open  C_Ast in

datatype data = data_bool of bool     | data_int of int 
              | data_string of string | data_absstring of string 

type node_content = { tag     : string,
                      sub_tag : string,
                      args    : data list }
                    
fun TT s = { tag = s, sub_tag = "", args = [] }
fun TTT s t = { tag = s, sub_tag = t, args = [] }
fun ({ tag,sub_tag,args} #>> S) = { tag = tag, sub_tag = sub_tag, args = args @ S }

fun fold_optiona _ None st = st | fold_optiona g (Some a) st = g a st;
fun fold_either g1 _ (Left a) st    = g1 a st
   |fold_either _ g2 (Right a) st   = g2 a st

fun toString_cStructTag (X:C_Ast.cStructTag) = @{make_string} X

fun toString_cIntFlag  (X:C_Ast.cIntFlag)    = @{make_string} X
                                             
fun toString_cIntRepr  (X:C_Ast.cIntRepr)    = @{make_string} X
                                             
fun toString_cUnaryOp  (X:C_Ast.cUnaryOp)    = @{make_string} X
                      
fun toString_cAssignOp (X:C_Ast.cAssignOp)   = @{make_string} X
                                             
fun toString_cBinaryOp (X:C_Ast.cBinaryOp)   = @{make_string} X
                                             
fun toString_cIntFlag  (X:C_Ast.cIntFlag)    = @{make_string} X
                                             
fun toString_cIntRepr  (X:C_Ast.cIntRepr)    = @{make_string} X

fun dark_matter x = XML.content_of (YXML.parse_body x)


fun toString_abr_string S = case  to_String_b_a_s_e S of 
                               ST X => dark_matter X
                             | STa S => map (dark_matter o Int.toString) S 
                                        |> String.concatWith "," 
                                        |> enclose "[" "]"
 
fun toString_nodeinfo (NodeInfo0 (positiona, (positiona', i), namea)) =
    let val Position0 (i1,abrS,i2,i3) = positiona;
        val Position0 (i1',abrS',i2',i3') = positiona';
        val Name0 X = namea;
    in  "<"^Int.toString i1^" : "^toString_abr_string abrS^" : "
        ^ Int.toString i2 ^" : " ^ Int.toString i3 ^ " : " ^ 
        Int.toString i1'^" : "^toString_abr_string abrS'^" : "
        ^ Int.toString i2' ^" : " ^ Int.toString i3' ^ "|" ^ Int.toString i ^"::"
        ^ Int.toString X ^ ">"
    end
   |toString_nodeinfo (OnlyPos0 (positiona, (positiona', i))) =
    let val Position0 (i1,abrS,i2,i3) = positiona;
        val Position0 (i1',abrS',i2',i3') = positiona';
    in  "<"^Int.toString i1^" : "^toString_abr_string abrS^" : "
        ^ Int.toString i2 ^" : " ^ Int.toString i3 ^ " : " ^ 
        Int.toString i1'^" : "^toString_abr_string abrS'^" : "
        ^ Int.toString i2' ^" : " ^ Int.toString i3' ^ "|" ^ Int.toString i ^ ">"
    end;
                                                       

fun toString_Chara (Chara(b1,b2,b3,b4,b5,b6,b7,b8)) = 
             let val v1 = (b1 ? (K 0)) (128)
                 val v2 = (b2 ? (K 0)) (64)
                 val v3 = (b3 ? (K 0)) (32)
                 val v4 = (b4 ? (K 0)) (16)
                 val v5 = (b5 ? (K 0)) (8)
                 val v6 = (b6 ? (K 0)) (4)
                 val v7 = (b7 ? (K 0)) (2)
                 val v8 = (b8 ? (K 0)) (1)
             in  String.implode[Char.chr(v1+v2+v3+v4+v5+v6+v7+v8)] end

(* could and should be done much more: change this on demand. *)
fun fold_cInteger g' (CInteger0 (i: int, r: cIntRepr, rfl:cIntFlag flags)) st =  
                     st |> g'(TT "CInteger0" 
                              #>> [data_int i,
                                   data_string (@{make_string} r),
                                   data_string (@{make_string} rfl)])
fun fold_cChar   g'  (CChar0(c : char, b:bool)) st          = 
                     st |> g' (TT"CChar0"
                               #>> [data_string (toString_Chara c),data_bool (b)])
  | fold_cChar   g'  (CChars0(cs : char list, b:bool)) st   = 
                     let val cs' = cs |> map toString_Chara 
                                      |> String.concat 
                     in  st |> g' (TT"CChars0" #>> [data_string cs',data_bool b]) end
fun fold_cFloat  g'  (CFloat0 (bstr: abr_string)) st          = 
                     st |> g' (TT"CChars0"#>> [data_string (@{make_string} bstr)])
fun fold_cString g' (CString0 (bstr: abr_string, b: bool)) st = 
                     st |> g' (TT"CString0"#>> [data_string (@{make_string} bstr), data_bool b])


fun fold_cConstant g (CIntConst0 (i: cInteger, a))  st = st |> fold_cInteger (fn x=>g x a) i
                                                            |> g (TT"CIntConst0") a  
  | fold_cConstant g (CCharConst0  (c : cChar, a))  st = st |> fold_cChar (fn x=>g x a) c
                                                            |> g (TT"CCharConst0") a   
  | fold_cConstant g (CFloatConst0 (f : cFloat, a)) st = st |> fold_cFloat (fn x=>g x a) f
                                                            |> g (TT"CFloatConst0") a   
  | fold_cConstant g (CStrConst0   (s : cString, a))st = st |> fold_cString (fn x=>g x a) s
                                                            |> g (TT"CStrConst0") a

fun fold_ident a g (Ident0(bstr : abr_string, i : int, ni: nodeInfo (* hack !!! *))) st = 
                   st |> g (TT "Ident0"  
                            #>> [data_string (@{make_string} bstr), 
                                 data_int i, 
                                 data_string (@{make_string} ni)
                                ]) a
(* |> fold_cString (fn x=>g x a)  *)
fun fold_cStringLiteral g (CStrLit0(cs:cString, a)) st =  st |> fold_cString (fn x=>g x a) cs
                                                             |> g (TT"CStrLit0") a 


fun  fold_cTypeSpecifier g (CAtomicType0 (decl : 'a cDeclaration, a)) st = 
                                  st |> fold_cDeclaration g decl |> g (TT"CAtomicType0") a
   | fold_cTypeSpecifier g (CBoolType0 a)     st = st |> g (TT"CBoolType0") a
   | fold_cTypeSpecifier g (CCharType0 a)     st = st |> g (TT"CCharType0") a 
   | fold_cTypeSpecifier g (CComplexType0 a)  st = st |> g (TT"CComplexType0") a 
   | fold_cTypeSpecifier g (CDoubleType0 a)   st = st |> g (TT"CDoubleType0") a 
   | fold_cTypeSpecifier g (CEnumType0(e: 'a cEnumeration, a)) st = 
                                              st |> fold_cEnumeration g e
                                                 |> g (TT"CEnumType0") a
   | fold_cTypeSpecifier g (CFloatType0 a)    st = st |> g (TT"CFloatType0") a
   | fold_cTypeSpecifier g (CInt128Type0 a)   st = st |> g (TT"CInt128Type0") a 
   | fold_cTypeSpecifier g (CIntType0 a)      st = st |> g (TT"CIntType0") a 
   | fold_cTypeSpecifier g (CLongType0 a)     st = st |> g (TT"CLongType0") a 
   | fold_cTypeSpecifier g (CSUType0 (su: 'a cStructureUnion, a))  st = 
                                              st |> fold_cStructureUnion g su
                                                 |> g (TT"CSUType0") a
   | fold_cTypeSpecifier g (CShortType0 a)    st = st |> g (TT"CShortType0") a 
   | fold_cTypeSpecifier g (CSignedType0 a)   st = st |> g (TT"CSignedType0") a 
   | fold_cTypeSpecifier g (CTypeDef0 (id:ident, a)) st = 
                                              st |>  fold_ident a g id
                                                 |>  g (TT"CTypeDef0") a 
   | fold_cTypeSpecifier g (CTypeOfExpr0 (ex: 'a cExpression, a)) st = 
                                              st |> fold_cExpression g ex 
                                                 |> g (TT"CTypeOfExpr0") a
   | fold_cTypeSpecifier g (CTypeOfType0 (decl: 'a cDeclaration, a)) st = 
                                              st |> fold_cDeclaration g decl 
                                                 |> g (TT"CTypeOfType0") a
   | fold_cTypeSpecifier g (CUnsigType0 a)    st = st |> g (TT"CUnsigType0") a
   | fold_cTypeSpecifier g (CVoidType0 a)     st = st |> g (TT"CVoidType0")  a


and  fold_cTypeQualifier g (CAtomicQual0 a) st = g (TT"CAtomicQual0") a st
   | fold_cTypeQualifier g (CAttrQual0 (CAttr0 (id,eL:'a cExpression list, a))) st = 
                                              st |> fold_ident a g id
                                                 |> fold(fold_cExpression g) eL
                                                 |> g (TT"CAttrQual0") a
   | fold_cTypeQualifier g (CConstQual0 a) st    = st |> g (TT"CConstQual0") a
   | fold_cTypeQualifier g (CNonnullQual0 a) st  = st |> g (TT"CNonnullQual0") a 
   | fold_cTypeQualifier g (CNullableQual0 a) st = st |> g (TT"CNullableQual0") a
   | fold_cTypeQualifier g (CRestrQual0 a) st    = st |> g (TT"CRestrQual0") a
   | fold_cTypeQualifier g (CVolatQual0 a) st    = st |> g (TT"CVolatQual0") a 

and  fold_cStatement g (CLabel0(id:ident, s:'a cStatement, 
                                aL: 'a cAttribute list, a)) st= 
                                  st |> fold_ident a g id
                                     |> fold_cStatement g s
                                     |> fold(fold_cAttribute g) aL
                                     |> g (TT"CLabel0") a 
   | fold_cStatement g (CCase0(ex: 'a cExpression, 
                               stmt: 'a cStatement, a)) st    = 
                                  st |> fold_cExpression g ex
                                     |> fold_cStatement g stmt
                                     |> g (TT"CCase0") a
   | fold_cStatement g (CCases0(ex1: 'a cExpression, 
                                ex2: 'a cExpression,
                                stmt:'a cStatement, a)) st   = 
                                  st |> fold_cExpression g ex1
                                     |> fold_cExpression g ex2
                                     |> fold_cStatement g stmt
                                     |> g (TT"CCases0") a
   | fold_cStatement g (CDefault0(stmt:'a cStatement, a)) st  = 
                                  st |> fold_cStatement g stmt
                                     |> g (TT"CDefault0") a
   | fold_cStatement g (CExpr0(ex_opt:'a cExpression optiona, a)) st = 
                                  st |> fold_optiona (fold_cExpression g) ex_opt
                                     |> g (TT"CExpr0") a
   | fold_cStatement g (CCompound0(idS : ident list, 
                                   cbiS: 'a cCompoundBlockItem list, a)) st = 
                                  st |> fold(fold_ident a g) idS
                                     |> fold(fold_cCompoundBlockItem g) cbiS
                                     |> g (TT"CCompound0") a
   | fold_cStatement g (CIf0(ex1:'a cExpression,stmt: 'a cStatement, 
                        stmt_opt: 'a cStatement optiona, a)) st = 
                                  st |> fold_cExpression g ex1
                                     |> fold_cStatement g stmt
                                     |> fold_optiona (fold_cStatement g) stmt_opt
                                     |> g (TT"CIf0") a
   | fold_cStatement g (CSwitch0(ex1:'a cExpression, 
                                 stmt: 'a cStatement, a)) st = 
                                  st |> fold_cExpression g ex1
                                     |> fold_cStatement g stmt
                                     |> g (TT"CSwitch0") a
   | fold_cStatement g (CWhile0(ex1:'a cExpression, 
                                stmt: 'a cStatement, b: bool, a)) st = 
                                  st |> fold_cExpression g ex1
                                     |> fold_cStatement g stmt
                                     |> g (TT"CWhile0" #>> [data_bool b]) a
   | fold_cStatement g (CFor0(ex0:('a cExpression optiona, 'a cDeclaration) either, 
                              ex1_opt: 'a cExpression optiona, 
                              ex2_opt: 'a cExpression optiona,
                              stmt: 'a cStatement, a)) st = 
                                  st |> fold_either (fold_optiona (fold_cExpression g)) 
                                                    (fold_cDeclaration g) ex0
                                     |> fold_optiona (fold_cExpression g) ex1_opt
                                     |> fold_optiona (fold_cExpression g) ex2_opt
                                     |> fold_cStatement g stmt
                                     |> g (TT"CFor0") a
   | fold_cStatement g (CGoto0(id: ident, a)) st = 
                                  st |>  fold_ident a g id
                                     |> g (TT"CGoto0") a
   | fold_cStatement g (CGotoPtr0(ex1:'a cExpression, a)) st = 
                                  st |> fold_cExpression g ex1 |> g (TT"CGotoPtr0") a
   | fold_cStatement g (CCont0 a)  st = st |> g (TT"CCont0") a
   | fold_cStatement g (CBreak0 a) st = st |> g (TT"CBreak0") a
   | fold_cStatement g (CReturn0 (ex:'a cExpression optiona,a)) st = 
                                  st |> fold_optiona (fold_cExpression g) ex |> g (TT"CReturn0") a
   | fold_cStatement g (CAsm0(_: 'a cAssemblyStatement, a)) st = 
                                  (* assembly ignored so far *)
                                  st |> g (TT"CAsm0") a

and fold_cExpression g (CComma0 (eL:'a cExpression list, a)) st = 
                                 st |> fold(fold_cExpression g) eL |> g (TT"CComma0") a
  | fold_cExpression g (CAssign0(aop:cAssignOp, 
                                 ex1:'a cExpression,
                                 ex2:'a cExpression,a)) st = 
                                  st |> fold_cExpression g ex1
                                     |> fold_cExpression g ex2 
                                     |> g (TTT"CAssign0" (toString_cAssignOp aop)) a
  | fold_cExpression g (CCond0(  ex1:'a cExpression, 
                                 ex2opt: 'a cExpression optiona, (* bescheuert ! Wieso option ?*) 
                                 ex3: 'a cExpression,a)) st = 
                                  st |> fold_cExpression g ex1 
                                     |> fold_optiona (fold_cExpression g) ex2opt
                                     |> fold_cExpression g ex3 |> g (TT"CCond0") a
  | fold_cExpression g (CBinary0(bop: cBinaryOp, ex1: 'a cExpression,ex2: 'a cExpression, a)) st =
                                  st |> fold_cExpression g ex1 
                                     |> fold_cExpression g ex2 
                                     |> g (TTT"CBinary0"(toString_cBinaryOp bop)) a 
  | fold_cExpression g (CCast0(decl:'a cDeclaration, ex: 'a cExpression, a)) st = 
                                  st |> fold_cExpression g ex 
                                     |> fold_cDeclaration g decl
                                     |> g (TT"CCast0") a
  | fold_cExpression g (CUnary0(unop:cUnaryOp, ex: 'a cExpression, a)) st = 
                                  st |> fold_cExpression g ex 
                                     |> g (TT("CUnary0 "^toString_cUnaryOp unop)) a
  | fold_cExpression g (CSizeofExpr0(ex:'a cExpression, a)) st = 
                                  st |> fold_cExpression g ex    |> g (TT"CSizeofExpr0") a
  | fold_cExpression g (CSizeofType0(decl:'a cDeclaration,a)) st = 
                                  st |> fold_cDeclaration g decl |> g (TT"CSizeofType0") a
  | fold_cExpression g (CAlignofExpr0(ex:'a cExpression, a)) st = 
                                  st |> fold_cExpression g ex    |> g (TT"CAlignofExpr0") a
  | fold_cExpression g (CAlignofType0(decl:'a cDeclaration, a)) st = 
                                  st |> fold_cDeclaration g decl |> g (TT"CAlignofType0") a
  | fold_cExpression g (CComplexReal0(ex:'a cExpression, a)) st = 
                                  st |> fold_cExpression g ex |> g (TT"CComplexReal0") a
  | fold_cExpression g (CComplexImag0(ex:'a cExpression, a)) st = 
                                  st |> fold_cExpression g ex |> g (TT"CComplexImag0") a
  | fold_cExpression g (CIndex0(ex1:'a cExpression, ex2: 'a cExpression, a)) st =
                                  st |> fold_cExpression g ex1 
                                     |> fold_cExpression g ex2 
                                     |> g (TT"CIndex0") a
  | fold_cExpression g (CCall0(ex:'a cExpression, argS: 'a cExpression list, a)) st =
                                  st |> fold_cExpression g ex 
                                     |> fold (fold_cExpression g) argS 
                                     |> g (TT"CCall0") a
  | fold_cExpression g (CMember0(ex:'a cExpression, id:ident, b, a)) st =
                                  st |> fold_cExpression g ex 
                                     |> fold_ident a g id
                                     |> g (TT"CMember0"#>> [data_bool b]) a 
  | fold_cExpression g (CVar0(id:ident,a)) st = st |> fold_ident a g id |> g (TT"CVar0") a
  | fold_cExpression g (CConst0(cc:'a cConstant)) st  = st |> fold_cConstant g cc
  | fold_cExpression g (CCompoundLit0(decl:'a cDeclaration,
                                      eqn: ('a cPartDesignator list * 'a cInitializer) list, a)) st =
                                  st |> fold(fn(S,init) => 
                                             fn st => st |> fold(fold_cPartDesignator g) S
                                                         |> fold_cInitializer g init) eqn 
                                     |> fold_cDeclaration g decl 
                                     |> g (TT"CCompoundLit0") a 
  | fold_cExpression g (CGenericSelection0(ex:'a cExpression, 
                                           eqn: ('a cDeclaration optiona*'a cExpression)list,a)) st =
                                  st |> fold_cExpression g ex 
                                     |> fold (fn (d,ex) => 
                                              fn st => st |> fold_optiona (fold_cDeclaration g) d  
                                                          |> fold_cExpression g ex) eqn  
                                     |> g (TT"CGenericSelection0") a  
  | fold_cExpression g (CStatExpr0(stmt: 'a cStatement,a)) st =  
                                  st |> fold_cStatement g stmt |> g (TT"CStatExpr0") a
  | fold_cExpression g (CLabAddrExpr0(id:ident,a)) st = 
                                  st |> fold_ident a g id |> g (TT"CLabAddrExpr0") a 
  | fold_cExpression g (CBuiltinExpr0(X: 'a cBuiltinThing)) st = st |> fold_cBuiltinThing g X
  
and fold_cDeclaration g (CDecl0(dsS : 'a cDeclarationSpecifier list, 
                                mkS: (('a cDeclarator optiona       
                                       *'a cInitializer optiona) 
                                     * 'a cExpression optiona) list,
                                a)) st = 
                                  st |> fold(fold_cDeclarationSpecifier g) dsS 
                                     |> fold(fn ((d_o, init_o),ex_opt) =>
                                             fn st => st |> fold_optiona(fold_cDeclarator g) d_o
                                                         |> fold_optiona(fold_cInitializer g) init_o
                                                         |> fold_optiona(fold_cExpression g) ex_opt) mkS
                                    |> g (TT"CDecl0") a
  | fold_cDeclaration g (CStaticAssert0(ex:'a cExpression, slit: 'a cStringLiteral, a)) st = 
                                  st |> fold_cExpression g ex 
                                     |> fold_cStringLiteral g slit
                                     |> g (TT"CStaticAssert0") a

and fold_cBuiltinThing g (CBuiltinVaArg0(ex:'a cExpression,decl: 'a cDeclaration,a)) st = 
                                  st |> fold_cExpression g ex 
                                     |> fold_cDeclaration g decl
                                     |> g (TT"CBuiltinVaArg0") a 
  | fold_cBuiltinThing g (CBuiltinOffsetOf0(d: 'a cDeclaration, _: 'a cPartDesignator list, a)) st = 
                                  st |> fold_cDeclaration g d 
                                     |> g (TT"CBuiltinOffsetOf0") a 
  | fold_cBuiltinThing g (CBuiltinTypesCompatible0 (d1: 'a cDeclaration, d2: 'a cDeclaration,a)) st= 
                                  st |> fold_cDeclaration g d1
                                     |> fold_cDeclaration g d2 
                                     |> g (TT"CBuiltinTypesCompatible0") a 

and  fold_cInitializer g (CInitExpr0(ex: 'a cExpression, a)) st = 
                                  st |> fold_cExpression g ex |> g (TT"CInitExpr0") a 
   | fold_cInitializer g (CInitList0 (mms: ('a cPartDesignator list * 'a cInitializer) list,a)) st = 
                                  st |> fold(fn (a,b) => 
                                             fn st => st|> fold(fold_cPartDesignator g) a 
                                                        |> fold_cInitializer g b) mms
                                     |> g (TT"CInitList0") a

and  fold_cPartDesignator g (CArrDesig0(ex: 'a cExpression, a)) st = 
                                  st |> fold_cExpression g ex |> g (TT"CArrDesig0") a 
   | fold_cPartDesignator g (CMemberDesig0(id: ident, a)) st = 
                                  st |> fold_ident a g id |> g (TT"CMemberDesig0") a 
   | fold_cPartDesignator g (CRangeDesig0(ex1: 'a cExpression, ex2: 'a cExpression, a)) st = 
                                  st |> fold_cExpression g ex1
                                     |> fold_cExpression g ex2
                                     |> g (TT"CRangeDesig0") a 

and fold_cAttribute g (CAttr0(id: ident, exS: 'a cExpression list, a)) st =
                                  st |> fold_ident a g id
                                     |> fold(fold_cExpression g) exS 
                                     |> g (TT"CAttr0") a 

and fold_cEnumeration g (CEnum0 (ident_opt: ident optiona,
                              exS_opt: ((ident * 'a cExpression optiona) list) optiona,
                              attrS: 'a cAttribute list, a)) st = 
                                  st |> fold_optiona(fold_ident a g) ident_opt
                                     |> fold_optiona(fold(
                                             fn (id,ex_o) =>
                                             fn st => st |> fold_ident a g id
                                                         |> fold_optiona (fold_cExpression g) ex_o)) 
                                                      exS_opt   
                                     |> fold(fold_cAttribute g) attrS
                                     |> g (TT"CEnum0") a 



and fold_cArraySize a g (CNoArrSize0 (b: bool)) st = 
                                  st |> g (TT "CNoArrSize0" #>> [data_bool b]) a 
  | fold_cArraySize a g (CArrSize0 (b:bool, ex : 'a cExpression)) st = 
                                  st |> fold_cExpression g ex 
                                     |> g (TT "CNoArrSize0" #>> [data_bool b]) a 


and fold_cDerivedDeclarator g (CPtrDeclr0 (tqS: 'a cTypeQualifier list , a)) st = 
                                  st |> fold(fold_cTypeQualifier g) tqS 
                                     |> g (TT"CPtrDeclr0") a
  | fold_cDerivedDeclarator g (CArrDeclr0 (tqS:'a cTypeQualifier list, aS: 'a cArraySize,a)) st = 
                                  st |> fold(fold_cTypeQualifier g) tqS 
                                     |> fold_cArraySize a g aS 
                                     |> g (TT"CArrDeclr0") a
  | fold_cDerivedDeclarator g (CFunDeclr0 (decl_alt: (ident list, 
                                                      ('a cDeclaration list * bool)) either, 
                                           aS: 'a cAttribute list, a)) st = 
                                  st |> fold_either 
                                             (fold(fold_ident a g)) 
                                             (fn (declS,b) =>
                                              fn st => st |> fold (fold_cDeclaration g) declS
                                                          |> g (TTT "CFunDeclr0""decl_alt-Right"
                                                                #>> [data_bool b]) a) decl_alt
                                     |> fold(fold_cAttribute g) aS 
                                     |> g (TT"CFunDeclr0") a

and fold_cDeclarationSpecifier g (CStorageSpec0(CAuto0 a)) st = 
                                  st |> g (TTT"CStorageSpec0" "CAuto0") a
   |fold_cDeclarationSpecifier g (CStorageSpec0(CRegister0 a)) st = 
                                  st |> g (TTT"CStorageSpec0" "CRegister0") a
   |fold_cDeclarationSpecifier g (CStorageSpec0(CStatic0 a)) st = 
                                  st |> g (TTT"CStorageSpec0" "CStatic0") a
   |fold_cDeclarationSpecifier g (CStorageSpec0(CExtern0 a)) st = 
                                  st |> g (TTT"CStorageSpec0" "CExtern0") a
   |fold_cDeclarationSpecifier g (CStorageSpec0(CTypedef0 a)) st = 
                                  st |> g (TTT"CStorageSpec0" "CTypedef0") a
   |fold_cDeclarationSpecifier g (CStorageSpec0(CThread0 a)) st = 
                                  st |> g (TTT"CStorageSpec0" "CThread0") a

   |fold_cDeclarationSpecifier g (CTypeSpec0(CVoidType0 a)) st = 
                                  st |> g (TTT"CTypeSpec0""CVoidType0") a
   |fold_cDeclarationSpecifier g (CTypeSpec0(CCharType0 a)) st = 
                                  st |> g (TTT"CTypeSpec0""CCharType0") a
   |fold_cDeclarationSpecifier g (CTypeSpec0(CShortType0 a)) st = 
                                  st |> g (TTT"TCTypeSpec0""CShortType0") a
   |fold_cDeclarationSpecifier g (CTypeSpec0(CIntType0 a)) st = 
                                  st |> g (TTT"CTypeSpec0""CIntType0") a
   |fold_cDeclarationSpecifier g (CTypeSpec0(CLongType0 a)) st = 
                                  st |> g (TTT"CTypeSpec0""CLongType0") a
   |fold_cDeclarationSpecifier g (CTypeSpec0(CFloatType0 a)) st = 
                                  st |> g (TTT"CTypeSpec0""CFloatType0") a
   |fold_cDeclarationSpecifier g (CTypeSpec0(CDoubleType0 a)) st = 
                                  st |> g (TTT"CTypeSpec0""CDoubleType0") a
   |fold_cDeclarationSpecifier g (CTypeSpec0(CSignedType0 a)) st = 
                                  st |> g (TTT"CTypeSpec0""CSignedType0") a
   |fold_cDeclarationSpecifier g (CTypeSpec0(CUnsigType0 a)) st = 
                                  st |> g (TTT"CTypeSpec0""CUnsigType0") a
   |fold_cDeclarationSpecifier g (CTypeSpec0(CBoolType0 a)) st = 
                                  st |> g (TTT"CTypeSpec0""CBoolType0") a

   |fold_cDeclarationSpecifier g (CTypeQual0(x: 'a cTypeQualifier)) st =
                                  st |> fold_cTypeQualifier g x

   |fold_cDeclarationSpecifier g (CFunSpec0(CInlineQual0 a)) st = 
                                  st |> g (TTT"CFunSpec0""CInlineQual0") a 
   |fold_cDeclarationSpecifier g (CFunSpec0(CNoreturnQual0 a)) st = 
                                  st |> g (TTT"CFunSpec0""CNoreturnQual0") a 
 
   |fold_cDeclarationSpecifier g (CAlignSpec0(CAlignAsType0(decl,a))) st = 
                                  st |> fold_cDeclaration g decl
                                     |> g (TTT"CAlignSpec0""CAlignAsType0") a
   |fold_cDeclarationSpecifier g (CAlignSpec0(CAlignAsExpr0(ex,a))) st = 
                                  st |> fold_cExpression g ex
                                     |> g (TTT"CAlignSpec0""CAlignAsType0") a

and fold_cDeclarator g (CDeclr0(id_opt: ident optiona,
                                declS: 'a cDerivedDeclarator list,
                                sl_opt: 'a cStringLiteral optiona,
                                attrS: 'a cAttribute list, a)) st = 
                                  st |> fold_optiona(fold_ident a g) id_opt
                                     |> fold (fold_cDerivedDeclarator g) declS
                                     |> fold_optiona(fold_cStringLiteral g) sl_opt
                                     |> fold(fold_cAttribute g) attrS
                                     |> g (TT"CDeclr0") a

and fold_cFunctionDef g (CFunDef0(dspecS: 'a cDeclarationSpecifier list,
                                  dclr: 'a cDeclarator,
                                  declsS: 'a cDeclaration list,
                                  stmt: 'a cStatement, a)) st = 
                                    st |> fold(fold_cDeclarationSpecifier g) dspecS
                                       |> fold_cDeclarator g dclr
                                       |> fold(fold_cDeclaration g) declsS
                                       |> fold_cStatement g stmt
                                       |> g (TT"CFunDef0") a

and fold_cCompoundBlockItem g (CBlockStmt0 (stmt: 'a cStatement)) st = 
                                    st |> fold_cStatement g stmt 
  | fold_cCompoundBlockItem g (CBlockDecl0 (decl : 'a cDeclaration)) st = 
                                    st |> fold_cDeclaration g decl 
  | fold_cCompoundBlockItem g (CNestedFunDef0(fdef : 'a cFunctionDef)) st = 
                                    st |> fold_cFunctionDef g fdef

and fold_cStructureUnion g (CStruct0(  ct : cStructTag, id_a: ident optiona, 
                                       declS_opt : ('a cDeclaration list) optiona,
                                       aS: 'a cAttribute list, a)) st = 
                                    st |> fold_optiona (fold_ident a g) id_a
                                       |> fold_optiona (fold(fold_cDeclaration g)) declS_opt
                                       |> fold(fold_cAttribute g) aS
                                       |> g (TTT "CStruct0" (toString_cStructTag ct)) a  

datatype 'a C11_Ast = 
           mk_cInteger              of C_Ast.cInteger         
         | mk_cStructTag            of C_Ast.cStructTag 
         | mk_cUnaryOp              of C_Ast.cUnaryOp  
         | mk_cAssignOp             of C_Ast.cAssignOp 
         | mk_cBinaryOp             of C_Ast.cBinaryOp 
         | mk_cIntFlag              of C_Ast.cIntFlag  
         | mk_cIntRepr              of C_Ast.cIntRepr  
         | mk_cConstant             of 'a C_Ast.cConstant      
         | mk_cStringLiteral        of 'a C_Ast.cStringLiteral 
         | mk_cArraySize            of 'a C_Ast.cArraySize     
         | mk_cAttribute            of 'a C_Ast.cAttribute     
         | mk_cBuiltinThing         of 'a C_Ast.cBuiltinThing  
         | mk_cCompoundBlockItem    of 'a C_Ast.cCompoundBlockItem   
         | mk_cDeclaration          of 'a C_Ast.cDeclaration         
         | mk_cDeclarationSpecifier of 'a C_Ast.cDeclarationSpecifier
         | mk_cDeclarator           of 'a C_Ast.cDeclarator          
         | mk_cDerivedDeclarator    of 'a C_Ast.cDerivedDeclarator   
         | mk_cEnumeration          of 'a C_Ast.cEnumeration         
         | mk_cExpression           of 'a C_Ast.cExpression          
         | mk_cInitializer          of 'a C_Ast.cInitializer         
         | mk_cPartDesignator       of 'a C_Ast.cPartDesignator      
         | mk_cStatement            of 'a C_Ast.cStatement           
         | mk_cStructureUnion       of 'a C_Ast.cStructureUnion
         | mk_cTypeQualifier        of 'a C_Ast.cTypeQualifier 
         | mk_cTypeSpecifier        of 'a C_Ast.cTypeSpecifier 


end

end (*struct *)

\<close>
*)




ML\<open>
val sep = String.fields (fn x => x = #" ") 
fun collect_ident_args {tag,sub_tag,args} a S = if "Ident0" = tag 
                                                then case args of 
                                                        C11_Ast_Lib.data_string XX::_ => S@(sep XX) 
                                                      | S => error("collect_ident_args" ^ (@{make_string} S))
                                                else S

val identS = fold(C11_Ast_Lib.fold_cDerivedDeclarator collect_ident_args) params []


\<close>




(*regarde 
     @{ML_file "../../../src_ext/l4v/src/tools/autocorres/type_strengthen.ML"} 
*)
 

print_theorems
find_theorems name:GCD

find_theorems name:ensures

section\<open>The Correctness-Proof\<close>



theorem (in pgcd) pgcd'_correct:
    "\<lbrace> \<lambda>\<sigma>. m > 0 \<and> n > 0 \<and> m < INT_MAX \<and> n < INT_MAX \<rbrace> pgcd' m n \<lbrace> \<lambda>res \<sigma>. res = gcd m n \<rbrace>!"
  unfolding pgcd'_def
  text\<open>Turning precond into explicit assumption\<close>
  apply(rule validNF_assume_pre)
  text\<open>Annotating loop with invariant and measure\<close>
  apply(subst whileLoop_add_inv   [  where I = "\<lambda>(a, b) s.   pgcd_inv a b m n"
                                     and   M = "\<lambda>((a, b),s). nat a + nat b"])
  proof(wp, fold INT_MAX_def, auto simp: pgcd_inv_def)
     fix a b show " gcd a b = gcd m n \<Longrightarrow> gcd a (b - a) = gcd m n"
          by (metis (no_types, hide_lams) gcd.commute gcd_diff1)
  next
     fix a b show " gcd a b = gcd m n \<Longrightarrow> gcd (a - b) b = gcd m n"
          by (simp add: gcd_diff1)
  qed

end