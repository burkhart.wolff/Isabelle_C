(******************************************************************************
 * Isabelle/C
 *
 * Copyright (c) 2018-2019 Université Paris-Saclay, Univ. Paris-Sud, France
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *
 *     * Neither the name of the copyright holders nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************)

chapter\<open>The Isabelle/C/AutoCorres Demonstrator\<close>

theory AutoCorres_Wrapper
  imports AutoCorres.AutoCorres
begin

section \<open>Generic Annotation Data\<close>
text\<open>One technique to treat annotations is to store them during the parsing process and to
     evaluate them later --- for example when a semantic backend has processed the abstract 
     syntax to a certain stage.\<close>

ML \<comment> \<open>\<^theory>\<open>Isabelle_C.C_Eval\<close>\<close> \<open>
(*  Author:     Frédéric Tuong, Burkhart Wolff, Université Paris-Saclay *)
(*  Based on:      Pure/Isar/args.ML ( Markus Wenzel, TU Muenchen)

Mechanism to register Annotations of different formats in an own table.
*)
structure C_Module' =
struct
type Anno_Data = {tag     : string, 
                  c_env   : C_Env.env_lang, 
                  label   : binding option, 
                  content : bstring}
structure Annotation_Data = Generic_Data
                             (type T = Anno_Data list Inttab.table
                              val empty = Inttab.empty
                              val extend = K empty
                              val merge = K empty)
end
\<close>

ML \<comment> \<open>\<^file>\<open>~~/src/Pure/Isar/args.ML\<close>, \<^ML_structure>\<open>C_Parse\<close>, \<^ML_structure>\<open>C_Token\<close>\<close>
(*  Author:     Frédéric Tuong, Burkhart Wolff Université Paris-Saclay *)
(*  Based on:   Pure/Isar/args.ML ( Markus Wenzel, TU Muenchen)

Quasi-inner syntax based on outer tokens: concrete argument syntax of
attributes, methods etc. based on C_Token and C_Parse.
*)
\<open>
structure C_Args =
struct

(** argument scanners **)

(* basic *)

val ident = C_Parse.token
  (C_Parse.short_ident || C_Parse.long_ident || C_Parse.sym_ident || C_Parse.term_var ||
    C_Parse.type_ident || C_Parse.type_var   || C_Parse.number);

val string = C_Parse.token C_Parse.string;

val cartouche = C_Parse.token C_Parse.cartouche;

val embedded_token = ident || string || cartouche;
val embedded_inner_syntax = embedded_token >> C_Token.inner_syntax_of;
end
\<close>

ML \<comment> \<open>\<^theory>\<open>Isabelle_C.C_Command\<close>\<close> \<open>
local
type text_range = Symbol_Pos.text * Position.T

datatype antiq_hol = Invariant of string (* term *)
                   | Fnspec of text_range (* ident *) * string (* term *)
                   | Relspec of string (* term *)
                   | Modifies of (bool (* true: [*] *) * text_range) list
                   | Dont_translate
                   | Auxupd of string (* term *)
                   | Ghostupd of string (* term *)
                   | Spec of string (* term *)
                   | End_spec of string (* term *)
                   | Calls of text_range list
                   | Owned_by of text_range

val scan_ident =    Scan.one (C_Token.is_kind Token.Ident) 
                 >> (fn tok => (C_Token.content_of tok, C_Token.pos_of tok))
val scan_brack_star = C_Parse.position (C_Parse.$$$ "[") 
                       -- C_Parse.star 
                       -- C_Parse.range (C_Parse.$$$ "]")
                      >> (fn (((s1, pos1), s2), (s3, (_, pos3))) => 
                                          (s1 ^ s2 ^ s3, Position.range_position (pos1, pos3)))

val scan_opt_colon = Scan.option (C_Parse.$$$ ":")
val scan_colon = C_Parse.$$$ ":" >> SOME

fun bind scan f ((stack1, (to_delay, stack2)), _) =
  C_Parse.range scan
  >> (fn (src, range) =>
      C_Env.Parsing
        ( (stack1, stack2)
        , ( range
          , C_Inner_Syntax.bottom_up (f src)
          , Symtab.empty
          , to_delay)))

fun command cmd scan0 scan f =
       C_Annotation.command' cmd "" (K (scan0 -- (scan >> f) >> K C_Env.Never))

fun command' cmd scan f =
       C_Annotation.command' cmd "" (bind scan f)

fun command'' cmd _ _ _ =
  command' cmd
           (C_Token.syntax' (Parse.token Parse.term))
           (fn src => fn _ => fn context =>
             ML_Context.exec
               (tap (fn _ => Syntax.read_term (Context.proof_of context)
                                              (Token.inner_syntax_of src)))
               context)



val _ = command' : string * Position.T ->
      (binding option * string) C_Parse.parser ->
        (binding option * string -> C_Env.env_propagation_reduce -> Context.generic -> Context.generic)
          -> theory -> theory

in

fun store_anno_expr tag (lab,src) _ gthy =
  let val l = length (C_Module.Data_In_Source.get gthy)
      val c_env :  C_Env.env_lang = (C_Module.get_last_env gthy)   (*last environment before C command *)
      val c_env :  C_Env.env_lang = C_Stack.Data_Lang.get_current_env gthy 
      val res = {c_env = c_env, content = src, label = lab, tag = tag}
  in  gthy |> C_Module'.Annotation_Data.map (Inttab.map_default (l, []) (cons res))  
  end

val _ = store_anno_expr :    string ->  binding option * bstring 
                         ->  C_Env.env_propagation_reduce 
                         ->  Context.generic -> Context.generic


val invariant = store_anno_expr "invariant" 
val measure   = store_anno_expr "measure" 
val requires  = store_anno_expr "requires" 
val ensures   = store_anno_expr "ensures" 

val scan_opt_label = Scan.option (C_Parse.binding --| C_Parse.$$$ ":")
val scan_lab_inner_syntax = (scan_opt_label -- C_Args.embedded_inner_syntax) 
                            : (binding option * bstring) c_parser



val bind_export = bind;

val _ = Theory.setup ((* 1 '@' *)
                      (* 'classic' AutoCorres annotations transferred via the AST *) 
                         command ("INVARIANT", \<^here>) scan_colon C_Parse.term Invariant
                      #> command ("INV", \<^here>) scan_colon C_Parse.term Invariant

                      (* '+' until being at the position of the first ident
                        then 2 '@' *)
                      #> command ("FNSPEC", \<^here>) scan_opt_colon (scan_ident --| scan_opt_colon -- C_Parse.term) Fnspec
                      #> command ("RELSPEC", \<^here>) scan_opt_colon C_Parse.term Relspec
                      #> command ("MODIFIES", \<^here>) scan_colon (Scan.repeat (   scan_brack_star >> pair true
                                                                          || scan_ident >> pair false))
                                                            Modifies
                      #> command ("DONT_TRANSLATE", \<^here>) scan_opt_colon (Scan.succeed ()) (K Dont_translate)

                      (**)
                      #> command ("AUXUPD", \<^here>) scan_colon C_Parse.term Auxupd
                      #> command ("GHOSTUPD", \<^here>) scan_colon C_Parse.term Ghostupd
                      #> command ("SPEC", \<^here>) scan_colon C_Parse.term Spec
                      #> command ("END-SPEC", \<^here>) scan_colon C_Parse.term End_spec

                      (**)
                      #> command ("CALLS", \<^here>) scan_opt_colon (Scan.repeat scan_ident) Calls
                      #> command ("OWNED_BY", \<^here>) scan_opt_colon scan_ident Owned_by

                      (* modern Isabelle_C versions based on post-parsing evaluation. *)

                      #> command' ("invariant", \<^here>) scan_lab_inner_syntax invariant
                      #> command' ("measure", \<^here>)   scan_lab_inner_syntax measure 
                      #> command' ("requires", \<^here>)  scan_lab_inner_syntax requires
                      #> command' ("ensures", \<^here>)   scan_lab_inner_syntax ensures);
end
\<close>



ML\<open>
fun Isabelle_C_AutoCorres_annotation_post_proc 
              (function_name: string) 
              (ts_def:thm) 
              (ts_info:FunctionInfo.function_info) lthy =
   let  val invariant =   Inttab.lookup (C_Module'.Annotation_Data.get (Context.Proof lthy))
                                              (length (C_Module.Data_In_Source.get (Context.Proof lthy)))
                       |> the_list
                       |> flat
                       |> List.partition (fn X => #tag X = "invariant") 
                       |> map (fn X => (true,#content X)) o fst
                              
        val measure    =  Inttab.lookup (C_Module'.Annotation_Data.get (Context.Proof lthy))
                                            (length (C_Module.Data_In_Source.get (Context.Proof lthy)))
                       |> the_list
                       |> flat
                       |> List.partition (fn X => #tag X = "measure")
                       |> map (fn X => (false,#content X)) o fst 
        in
             (if length invariant + length measure > 0 then
               Utils.define_lemma
                 (function_name ^ "_annotated")
                 (ts_def
                  |> Rule_Insts.of_rule
                        lthy
                        (map (fst #> SOME) (#args ts_info), [])
                        []
                  |> asm_full_simplify
                       (clear_simpset
                         lthy
                         addsimps
                         [ @{thm whileLoopE_add_inv}
                           |> Rule_Insts.where_rule
                               lthy
                               let fun add (var, pos) = map (apfst (K ((var, 0), pos)))
                               in add ("I", \<^here>) invariant @ add ("M", \<^here>) measure end
                               [] ]))
                 lthy
               |> #2
             else
               lthy)
        end


(* and here we set into the AutoCorres-Compiler, after having done the mapping 
   to the abstract memory model, our own post-processor. Thus, the post-processor 
   has access to the final types of arguments, global variables, names of calls ... *) 
val _ = TypeStrengthen.set_annotator Isabelle_C_AutoCorres_annotation_post_proc;

\<close>


end
